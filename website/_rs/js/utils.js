// Download manager
// Url-to-object and url-to-id manager

function download(path, onSuccess, onError = null)
{
	let promise = new Promise(function (resolve, reject)
	{
		let request;
		if (window.XMLHttpRequest)
			request = new XMLHttpRequest();
		else
			request = new ActiveXObject("Microsoft.XMLHTTP");

		request.open("GET", path);
		request.onreadystatechange = function()
		{
			if (request.status === 200){
				if(request.readyState === 4)
				{
					resolve(onSuccess(request));
				}
			}

			else reject(function(){
				if (onError != null)
					onError();
				else
					console.error(request.status);
			});
		}
		request.send();
	});
}

function downloadJson(path, onSuccess, onError = null)
{
	download(path, function(result)
	{
		onSuccess(JSON.parse(result.responseText));
	}, onError);
}

function houseUrlToObject(houseUrl)
{
	// Getting house id
	let houseUrlId = urlToId(houseUrl);

	// Todo: check by localStorage instead
	for(let house of houses)
	{
		let id = house.url.split('/');
		id = id[id.length - 1];

		if(houseUrlId == id)
			return house;
	}

	console.error("This house is not downloaded:", houseUrl);
	return null;
}

function urlToId(url)
{
	let urlId = url.split('/');
	return urlId[urlId.length - 1];
}

// HTML Canvas resolution setup
function initCanvas(canvasElement)
{
  let dpr = window.devicePixelRatio || 1;
  let rect = canvasElement.getBoundingClientRect();
  canvasElement.width = rect.width * dpr;
  canvasElement.height = rect.height * dpr;
  canvasElement.getContext('2d').scale(dpr, dpr);
}

function distance(pt1, pt2)
{
	/*let a = pt1.x - pt2.x;
	let b = pt1.y - pt2.y;
	return Math.sqrt(a*a + b*b);*/
	return Math.hypot(pt2.x-pt1.x, pt2.y-pt1.y);
}