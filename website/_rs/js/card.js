class Card 
{
	constructor(character, house)
	{
		this.name = character.name;
		this.houseName = house.name;
		this.houseNameShort = this.houseName.split(' ')[1];
		this.title = character.titles; // Todo: format this

		this.characterId = urlToId(character.url);
		this.houseId = urlToId(house.url);

		this.character = character;
		this.house = house;

		this.stepIndex = 0;
		this.position = {x: 0, y: 0};

	}
}

function getCardElement(card, clickEvent = true)
{
	let cardElement = document.createElement("div");
	cardElement.classList.add("card");
	cardElement.dataset.id = card.characterId;
	cardElement.dataset.selected = 0;
	if (clickEvent)
		cardElement.setAttribute("onclick", "onCardClick(this);");

	cardElement.innerHTML = `
		<img class="card__base" 
		 src="_rs/images/cards/`+card.houseNameShort+`.png" alt="`+card.houseNameShort+` card base">
		<img class="card__portrait" 
		 src="_rs/images/characters/`+card.name+`.jpg" alt="`+card.name+` portrait">
		<div class="card__name">`+card.name+`</div>
		<div class="card__details">
			<div class="card__details__title"></div>
			<div class="card__details__house">`+card.houseName+`</div>
		</div>
	`;

	return cardElement;
}